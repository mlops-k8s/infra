## 0. Prerequisites

    swapoff -a
    # go to /etc/fstab and disable swapfile

## 1. Packages

    sudo apt-get -y install apt-transport-https ca-certificates software-properties-common curl
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
    sudo apt update && sudo apt install -y docker-ce kubelet kubeadm kubernetes-cni

## 2. Kubernetes install

First, configure systemd as cgroups driver

    cat > /etc/docker/daemon.json <<EOF
    {
      "exec-opts": ["native.cgroupdriver=systemd"],
      "log-driver": "json-file",
      "log-opts": {
        "max-size": "100m"
      },
      "storage-driver": "overlay2"
    }
    EOF

And restart docker. Then:

    sudo kubeadm init
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    kubectl taint nodes --all node-role.kubernetes.io/master-
    kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

## 3. Nginx ingress

First, download latest manifest:

    wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.40.1/deploy/static/provider/baremetal/deploy.yaml -O k8s/nginx-deploy.yaml

Then add in the service definition

    spec:
      externalIPs:
      - 167.86.83.135

And apply: 

    kubectl apply -f k8s/nginx-deploy.yaml

## 4. Cert manager

Install cert manager:

    kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.2/cert-manager.yaml

Then create 2 issuers:

    kubectl apply -f k8s/staging_issuer.yaml
    kubectl apply -f k8s/prod_issuer.yaml 

## 5. Prometheus

    git clone https://github.com/prometheus-operator/kube-prometheus.git
    kubectl create -f manifests/setup
    kubectl create -f manifests

## 6. Local volumes

    sudo mkdir /volumes
    kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
    kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'

## 7. Grafana

Datasources:

    cat k8s/grafana-datasources.json | base64 -w 0
    kubectl edit secret -n monitoring grafana-datasources

Ingress:

    kubectl create secret generic -n kube-system oauth2-proxy-secrets \
        --from-literal=OAUTH2_PROXY_CLIENT_ID=... \
        --from-literal=OAUTH2_PROXY_CLIENT_SECRET=... \
        --from-literal=OAUTH2_PROXY_COOKIE_SECRET=...
    kubectl apply -f k8s/oauth2-proxy.yaml
    kubectl apply -f k8s/grafana-ingress.yaml

Persistence:

    kubectl apply -f k8s/grafana-persistence.yaml
    kubectl patch deployment grafana -n monitoring --type "json" -p '[{"op": "replace", "path": "/spec/template/spec/volumes/0", "value":{"name": "grafana-storage", "persistentVolumeClaim": {"claimName": "grafana"}}}]'


## 8. InfluxDB remote access

    docker run --rm -ti xmartlabs/htpasswd influx-remote ... > /tmp/auth
    kubectl create secret -n pv generic influx-remote-auth --from-file=/tmp/auth
    rm /tmp/auth
    kubectl apply -f k8s/influx-remote.yaml
