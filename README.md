# Infra

## Pre-setup

1. Login as root and add new user `adduser mario; usermod -G sudo mario`
2. Use `visudo` and change `%sudo   ALL=(ALL:ALL) ALL` to `%sudo   ALL=(ALL:ALL) NOPASSWD: ALL`
3. Logout and use `ssh-copy-id` to transfer public ssh key of `mario`

## Ansible

1. Install pip: `sudo apt install python3-pip git`
2. Install ansible: `sudo pip3 install ansible`
3. Set a proper hostname: `ansible localhost -m hostname -a name=c0.mlops.eu -b`
4. Fix `/etc/hosts` to point to the right hostname
